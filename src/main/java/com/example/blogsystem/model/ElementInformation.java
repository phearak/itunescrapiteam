package com.example.blogsystem.model;

public class ElementInformation {
	private String provider;
	private String size;
	public ElementInformation(String provider, String size) {
		super();
		this.provider = provider;
		this.size = size;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
    
    
}
