package com.example.blogsystem.model;

public class ElementItem {
	private String idImage;
	private String partImage;
    private String imgTitle;
    private String imgSubTitle;
    
    
	public ElementItem(String idImage, String partImage, String imgTitle, String imgSubTitle) {
		this.idImage = idImage;
		this.partImage = partImage;
		this.imgTitle = imgTitle;
		this.imgSubTitle = imgSubTitle;
	}
	
	public String getImgSubTitle() {
		return imgSubTitle;
	}

	public void setImgSubTitle(String imgSubTitle) {
		this.imgSubTitle = imgSubTitle;
	}

	public String getIdImage() {
		return idImage;
	}

	public void setIdImage(String idImage) {
		this.idImage = idImage;
	}

	public String getPartImage() {
		return partImage;
	}
	public void setPartImage(String partImage) {
		this.partImage = partImage;
	}
	public String getImgTitle() {
		return imgTitle;
	}
	public void setImgTitle(String imgTitle) {
		this.imgTitle = imgTitle;
	}
    
    
}
