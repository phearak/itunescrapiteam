package com.example.blogsystem.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.blogsystem.model.ElementItem;
import com.example.blogsystem.model.ExcelGenerator;

@Controller
public class ScrapController {
	@GetMapping("/getData")
	@ResponseBody
	public ResponseEntity<InputStreamResource> excelCustomersReport () throws IOException {
		String[]  columns = { "ID Image", "Image part", "Title", "Sub Title" };
		List<ElementItem> imElementItems = new ArrayList<>();
		final String WEBSITE 	= "https://www.apple.com/uk/itunes/charts/paid-apps/";
		Document douDocument 	= null;
		ByteArrayInputStream in = null;
		HttpHeaders headers 	= null;
		
		try {
			douDocument = Jsoup.connect(WEBSITE).get();
			// Get all content in side of li
			Elements allContent = douDocument.select(".section-content > ul > li");
			
			// Get id number in content
			Elements links = allContent.select("strong");
			// Get image part in content
			Elements imgs = allContent.select("img");
			// Get title in content
			Elements titles = allContent.select("h3");
			// Get sub title in content
			Elements subTitles = allContent.select("h4");
			for (int i = 0; i < allContent.size(); i++) {
				// get id
				String id = links.get(i).html();
				// get image part
				String img = imgs.get(i).attr("src");
				// get title
				String title = titles.get(i).text();
				// get sub title
				String subTitle = subTitles.get(i).text();
				// add all each content to array
				imElementItems.add(new ElementItem(id, img, title, subTitle)); 
			}
			
			List<String[]> list = new ArrayList<>();
			for (int i = 0;i < imElementItems.size();i++){
				String[] arr = {
						imElementItems.get(i).getIdImage(),
						imElementItems.get(i).getPartImage(),
						imElementItems.get(i).getImgTitle(),
						imElementItems.get(i).getImgSubTitle()
				};
				list.add(arr);
			}
			
			in = ExcelGenerator.customersToExcel(list,columns);
			
			headers = new HttpHeaders();
			headers.add("Content-Disposition", "attachment; filename=ItuneScrapIteam.xlsx");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Could not scan an item.");
		}

        return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
	}
}
