package com.example.blogsystem.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.blogsystem.model.ElementInformation;
import com.example.blogsystem.model.ExcelGenerator;

@Controller
public class ScrapInformationController {
	@GetMapping("/getInformation")
	@ResponseBody
	public ResponseEntity<InputStreamResource> excelCustomersReport () throws IOException {
		//String[]  columns = { "Provider", "Size", "Category", "Compatibility", "Languages", "Age Rating ", "Copyright", "Price" };
		String[]  columns 		= { "Information", ""};
		List<ElementInformation> elementInformations = new ArrayList<>();
		final String URLSITE 	= "https://apps.apple.com/gb/app/driving-theory-test-4-in-1-kit/id829581836?v0=WWW-EUUK-ITSTOP100-PAIDAPPS&l=en&ign-mpt=uo%3D4";
		Document douDocument 	= null;
		ByteArrayInputStream in = null;
		HttpHeaders headers 	= null;
		
		try {
			douDocument = Jsoup.connect(URLSITE).get();
			// Get all items details
			Elements allDetailInfo  = douDocument.select(".information-list--app > .information-list__item");
			
			// Get each element in class
			Elements titles = allDetailInfo.select(".information-list__item__term");
			Elements details = allDetailInfo.select(".information-list__item__definition");
			
			for (int i = 0; i < allDetailInfo.size(); i++) {
				// Get title each element
				String title = titles.get(i).html();
				// get details element with Disallowed tag jsoup
				String detail = details.get(i).wholeText();
				// Disallowed space in element (trim)
				
				// add all each content to array
				elementInformations.add(new ElementInformation(title, Jsoup.clean(detail, Whitelist.relaxed()))); 
			}
			// Add data to excel
			List<String[]> list = new ArrayList<>();
			for (int i = 0;i < elementInformations.size();i++){
				String[] arr = {
						elementInformations.get(i).getProvider(),
						elementInformations.get(i).getSize()
				};
				list.add(arr);
			}
			
			in = ExcelGenerator.customersToExcel(list,columns);
			
			headers = new HttpHeaders();
			headers.add("Content-Disposition", "attachment; filename=ItuneScrapInformationIteam.xlsx");
			
		} catch (Exception e) {
			System.out.println("Could not find information");
		}
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
	}
}
