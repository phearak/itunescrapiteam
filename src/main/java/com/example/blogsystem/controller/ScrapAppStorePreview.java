package com.example.blogsystem.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.blogsystem.model.ExcelGenerator;

@Controller
public class ScrapAppStorePreview {
	@GetMapping("/getAppStorePreview")
	@ResponseBody
	public ResponseEntity<InputStreamResource> excelCustomersReport () throws IOException {
		String[]  columns = {"Driving Theory Test 4 in 1 Kit"};

		final String URLSITE = "https://apps.apple.com/gb/app/driving-theory-test-4-in-1-kit/id829581836?v0=WWW-EUUK-ITSTOP100-PAIDAPPS&l=en&ign-mpt=uo%3D4";

		Document douDocument 	= null;
		ByteArrayInputStream in = null;
		HttpHeaders headers 	= null;
		String image 			= "";
        String title 			= "";
        String subTitle 		= "";
        String fromStudio 		= "";
        String rank 			= "";
        String rate 			= "";
        String price 			= "";
        List<String> item = new ArrayList<String>();
        List<String[]> list = new ArrayList<>();
        
		try {
			douDocument = Jsoup.connect(URLSITE).get();
			// Scrap Main Content
            image 		= douDocument.select("img").attr("src");
            title 		= douDocument.select("h1.product-header__title.app-header__title").text();
            subTitle 	= douDocument.select("h2.app-header__subtitle").text();
            fromStudio	= douDocument.select("h2.app-header__identity a").text();
            rank 		= douDocument.select("ul.app-header__list li").first().text();
            rate 		= douDocument.select("ul.app-header__list li.app-header__list__item--user-rating").text();
            price 		= douDocument.select("ul.app-header__list li.app-header__list__item--price").text();
            // Clear £ if there're any
            price 		= price.replace("£", "");  
            
            item.add(image);
            item.add(title);
            item.add(subTitle);
            item.add(fromStudio);
            item.add(rank);
            item.add(rate);
            item.add(price);
            // Add data to excel
        	for(String oneItem: item) {
        		String[] arrItems = {
        			oneItem.toString()
        		};
        		list.add(arrItems);
        	}
			
			in = ExcelGenerator.customersToExcel(list,columns);
			// Set data to excel
			headers = new HttpHeaders();
			headers.add("Content-Disposition", "attachment; filename=ItuneScrapInformationIteam.xlsx");
		} catch (Exception e) {
			System.out.println("Could not find information");
		}
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
	}
}
